from django.db.models import QuerySet #will import the Queryset class to checks if the object is an instance of the class
from json import JSONEncoder
from datetime import datetime


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet): #if o (Conference object) is an instance of a QuerySet, just turn it into
            return list(o)          # a normal list
        else:
            return super().default(o)

class DateEncoder(JSONEncoder): #converts any attribute that might have a datetime object into isoformat string so it it represented correctrly in the json output
    def default(self, o):
        if isinstance(o, datetime):    # if o is an instance of datetime
            return o.isoformat()
        else:
            return super().default(o)
#note we need to add DateEncoder before JSONEncoder in ModelEncoder

#model encoder inheriting from dateencoder(parentclass), etc. dateencoders changes the starts format into a format called ISOformat string that JSON will be able to take
class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder): #modelEncoder is the superclass that passes these funciton/properties to LocationDetailEncoder since ModelEncoder is  inheriting those functions/properties
    encoders = {}                               #self inside defaul is the instance of ModelEncoder, but self in this case will refer to the subclass ConferenceListEncoder(inherits properties of superclass, ModelEncoder) since it inheriting everything inside ModelEncoder, including self. o is the object(instance) conference that will be passed to the default method
    def default(self, o):                       # the first IF checks if the incoming object(ONE!conference instance) stored in the o parameter is an....
        if isinstance(o, self.model):           #...instance of the class Conference specified by self.model. ***The value Conference(our class) is called when we type self.model
            d = {}                              #will hold the property names as keys and the property values as values
            if hasattr(o,"get_api_url"):        # if the incoming o (object conference) has the attribute/method get_api_url...
                d["href"] = o.get_api_url()     #...then add the new key: value pair to the dictionary with the key "href". It finds the method in that instance of Conference so its True
            for property in self.properties:    #for each property in the properties list.. .properties come from the list in the api_views in ConferenceDetailEncoder. Self represents an instance of ConfereceListEncoder, since it inherits everythin that is inside Model Encoder. Similar to typing ConferenceDetailEncoder.properties to access the values such as "name", "description", etc..
                value = getattr(o, property)    # get the value of that property from the model instance given just the property name
                if property in self.encoders:  #when it gets to location(property) in properties it checks if its inside self.encoders(similar ConferenceDetailEncoder.encoders), which it is
                    encoder = self.encoders[property] #IT passes the value, LocationListEncoder().  key:value == "locatioin" : LocationListEncoder().
                    value = encoder.default(value) #IT then applies the encoder's DEFAULT method(default(self,o)) to the property value,LocationListEncoder().
                d[property] = value             # add a new key:value pair in the dictionary
            d.update(self.get_extra_data(o))   #edge case self.get_extra_data(o), the code is essentially providing a way for SUBCLASSES to inject additional data into the JSON representation of the object.
                                                #....This method can be overridden in subclasses of ModelEncoder to provide custom extra data specific to those subclasses.
            return d                            #return the dictionary
        else:                                   # otherwise,
            return super().default(o)           # From the documentation

    def get_extra_data(self, o):  #edge case overriden by the subclass. so its replaced.
        return {}


#we first crerated the model encoder >> dateencoder >>queryset encoder


# encoders = {} is used to store the encoders that are found in  subclasses such as ConferenceDetailEncoder. It is a placeholder
#     ****VERY Important of how the subclass overrides the superclass*******
# When you define encoders = {} in a subclass of ModelEncoder, you're essentially overriding the encoders attribute of the superclass.
# in this case the encoders in the subclass ConferenceDetailEncoder encoders = {"location": LocationListEncoder()} overrides...
# ...encoders ={} from the superclass ModelEncoder. We have to remember that ConferenceDetailEncoder is the one inheriting so within itself it can override anything that comes from the superclass


# d.update(self.get_extra_data(o), the code is essentially providing a way for SUBCLASS such as LocationDetailEncoder to inject additional data into the JSON representation of the object.
#This method can be overridden in subclasses of ModelEncoder to provide custom extra data specific to those subclasses.
#the function then return a dic,{"state": o.state.abbreviation},  that is added to the d dictionary .
