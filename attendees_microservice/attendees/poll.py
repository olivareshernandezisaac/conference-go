import json
import requests

from .models import ConferenceVO

#polls the monolith for data
def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url) #gets data in  JSON format
    content = json.loads(response.content)  #converts to python dic
    for conference in content["conferences"]: #adds import_href and a "name" : conference name
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"], #gets the value of conference["href"] and sets it as value for import_href key.
            defaults={"name": conference["name"]},
        )


#
# retrieves Conference data from the Monolith Conference API and update or create corresponding ConferenceVO objects in the Django database.

# Let's break down the code:

# import json: This imports Python's built-in json module, which provides functions for
# working with JSON data.

# import requests: This imports the requests module, which is a popular HTTP
# library for making requests to web servers.

# from .models import ConferenceVO: This imports the ConferenceVO model
# from the Django application's models.

# def get_conferences():: This defines a function named get_conferences
# that will retrieve conference data from the external API and update/create ConferenceVO objects.

# url = "http://monolith:8000/api/conferences/": This sets the URL of the
# API endpoint from which conference data will be retrieved.

# response = requests.get(url): This makes an HTTP GET request to the
# specified URL and stores the response.

# content = json.loads(response.content): This parses the JSON content
# of the response into a Python dictionary using the json.loads() function.

# for conference in content["conferences"]:: This iterates over each
# conference in the retrieved content.

# ConferenceVO.objects.update_or_create(...): This is a Django ORM
# method that either updates an existing ConferenceVO object if it already exists (based on the provided criteria) or creates a new one if it doesn't exist. In this case, it updates...
#.. or creates a ConferenceVO object based on the import_href field, setting its name attribute to the corresponding conference's name.


# Ex. conferences will look like this after json.loads transorms it into a pyhon dict
# {
#     "conferences": [
#         {
#             "href": "/api/conferences/1/",
#             "name": "Conference A"
#         }
# }

# then after the update_or_create it will look like

# ConferenceVO object for
#"Conference A":
# import_href: "/api/conferences/1/"
# name: "Conference A"
