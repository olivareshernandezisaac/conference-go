from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO
# from events.models import Conference : removed since Conference model is in the monolith, which will have its own container
from django.views.decorators.http import require_http_methods
import json

class ConferenceVODetailEncoder(ModelEncoder): #added since we have a new model to bring data from event.Conference in that is in the Monolith
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name",]

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None): # conference_vo_id=None replaces conference_id
    if request.method == "GET": #gets all the attendees for a specific conference
        attendees = Attendee.objects.filter(conference=conference_vo_id)#conference_vo_id replaces conference_id
        return JsonResponse(
            {"attendees": attendees},
            encoder = AttendeeListEncoder,
        )
    else: #an Attendee object will be created and added to the correct conference
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/" # conference_href variable, constructs a specific URL representing the conference to which the attendee will be added.

            conference = ConferenceVO.objects.get(import_href=conference_href)   #This method  performs a database query to retrieve a single object that matches the specified filter criteria. In this case, it filters objects based on the import_href field being equal to the value stored in the conference_href variable. . We have ConferenceVO since events.Conference class can't be accessed directly since it is in another bounded contex/container.
            content["conference"] = conference #Get the Conference object and adds it to the content dict.
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content) #it creates a new attendee with the person's name, the email, and the conferenceVO(which has "name" of conference and "import_href")
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    """^^^
    old code
    def api_list_attendees(request, conference_id):
        if request.method == "GET":
            attendees = Attendee.objects.filter(conference=conference_id)
            return JsonResponse(
                {"attendees": attendees},
                encoder = AttendeeListEncoder,
            )

        else:
            content = json.loads(request.body)

            # Get the Conference object and put it in the content dict
            try:
                conference = Conference.objects.get(id=conference_id)
                content["conference"] = conference
            except Conference.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid conference id"},
                    status=400,
                )

            attendee = Attendee.objects.create(**content)
            return JsonResponse(
                attendee,
                encoder=AttendeeDetailEncoder,
                safe=False,
            )



    older code
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.
    old code
    response = []
    attendees = Attendee.objects.filter(conference=conference_id)
    for attendee in attendees:
        response.append(
            {
                "name": attendee.name,              #"name": attendee's name,
                "href": attendee.get_api_url(),     #"href": URL to the attendee,
            }
        )
    return JsonResponse({"attendees": response})
    """

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {#tells the ModelEncoder that we want to use that specific encoder for the "conference" property since conference is its own object
        "conference":ConferenceVODetailEncoder()  #ConferenceVODetailEncoder()   return the name and import_href
    }

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder = AttendeeDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    """ ^^ old code
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.
    attendee = Attendee.objects.get(id=id)
    return JsonResponse({
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created,
        "conference": {
            "name": attendee.conference.name,
            "href": attendee.conference.get_api_url(),
        }
    })
     """
