from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

#system Design map on 7.6 b in Django notes
class ConferenceVO(models.Model): #added ConfereceVO to replace the Conference Model that is now in another bounded contex(monolith)
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Attendee(models.Model):
    """
    The Attendee model represents someone that wants to attend
    a conference
    """

    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    conference = models.ForeignKey(
        "ConferenceVO",                #we changed  "events.Conference", to "ConferenceVO" after it became a microservice
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def create_badge(self):  #3 making sure taht we use the aggregate root(Attendee) to be able to access Badge(Value Ojbect)
        try:
            badge = self.badge         # Attempt to retrieve an existing badge for the attendee. Assuming 'badge' is the related_name for the OneToOneField in the Attendee model.
        except ObjectDoesNotExist:              #Otherwise, create (and save, if necessary) a Badge instance with
            Badge.objects.create(attendee=self) #self as the value for the attendee property of the Badge.


    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"id": self.id})




class Badge(models.Model):
    """
    The Badge model represents the badge an attendee gets to
    wear at the conference.

    Badge is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )


# code  provided attempts to access self.create_badge, which is not an attribute or
# method that exists by default on the object. Therefore, if self.create_badge doesn't
# exist, it will indeed raise an AttributeError, not ObjectDoesNotExist.
