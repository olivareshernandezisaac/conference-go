from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
from events.api_views import ConferenceListEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference"
    ]
    encoders = {
        "conference": ConferenceListEncoder(), #conference encoder  return the name, id and href since
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}

##start of list presentation
class PresentaionListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]
    def get_extra_data(self, o):                    # gets the extra data, the state's abbreviation,
        return {"status": o.status.name}

@require_http_methods(["GET", "POST"]) #it only allows GET and POST request in this method
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
            {"presentation": presentations},
            encoder = PresentaionListEncoder
        )
    else:
        content = json.loads(request.body) #in insomnia we will type a JSON string that can have  presenter_name, company_name, presenter_email, status etc.
        try: # then we Get the specific  Conference object by its id and put it in the content dict
            conference = Conference.objects.get(id=conference_id) #we get the specific conference object by the id
            content["conference"] = conference #we add the conference object to our presentation
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        print(content) # we can see what is inside the content for the presentation POST in our terminal
        presentation = Presentation.create(**content) #model Presentation already has objects within the create function in the presentation model, that's why we dont need .objects
        return JsonResponse(  #in our resposnse in insomnia we will only see in conference its name, id because the ConferenceListEncoder(called as an encoder in the ...
                                #...PresentationDetailEncoder) only has those two properties and href because the ModelEncoder in json.py checks to see if the object, o(Conference in this case),
                                #..has the attribute "get_api_url", then it adds it to the python dictionary that it is being put together there. get_extra_data in PresentationDetailEncoder overrides the one in the json.py and status is added to the dictionary
            presentation,                   #...we will also get everything that is inside the PresentationDetailEncoder in JSON Format(still python) ...
            encoder=PresentationDetailEncoder,  #...and then JsonResponse(internally, can't see it) will take that python and convert it to JSON string to then send a response
            safe=False,
        )
    """^^  old code:
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.
    old code :
    presentations = [
        {
            "title": p.title,           #"title": presentation's title,
            "status": p.status.name,    #"status": presentation's status name
            "href": p.get_api_url(),    #"href": URL to the presentation,
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]
    return JsonResponse({"presentations": presentations})
    """
###end list presentation

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder = PresentationDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    """ old code ^^ goes with presentationDetailEncoder
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL
old code..
detail = Presentation.objects.get(id=id)
return JsonResponse(
    {
      "presenter_name": detail.presenter_name,  ##"presenter_name": the name of the presenter,
       "company_name": detail.company_name,     ## "company_name": the name of the presenter's company,
        "presenter_email": detail.presenter_email, #"presenter_email": the email address of the presenter,
        "title": detail.title,                  ###"title": the title of the presentation,
        "synopsis": detail.synopsis,                ##"synopsis": the synopsis for the presentation,
        "created": detail.created,             ###"created": the date/time when the record was created,
        "status": detail.status.name,           ###"status": the name of the status for the presentation,
        "conference": {
            "name": detail.conference.name,             ##"name": the name of the conference,
            "href": detail.conference.get_api_url(),    ##"href": the URL to the conference,
        }
    })
    """
