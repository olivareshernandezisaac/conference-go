from django.db import models
from django.urls import reverse


class Status(models.Model):
    """
    The Status model provides a status to a Presentation, which
    can be SUBMITTED, APPROVED, or REJECTED.

    Status is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"  # Fix the pluralization


class Presentation(models.Model):
    """
    The Presentation model represents a presentation that a person
    wants to give at the conference.
    """

    presenter_name = models.CharField(max_length=150)
    company_name = models.CharField(max_length=150, null=True, blank=True)
    presenter_email = models.EmailField()

    title = models.CharField(max_length=200)
    synopsis = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    status = models.ForeignKey(
        Status,
        related_name="presentations",
        on_delete=models.PROTECT,
    )

    conference = models.ForeignKey(
        "events.Conference",
        related_name="presentations",
        on_delete=models.CASCADE,
    )

    def approve(self):
        status = Status.objects.get(name="APPROVED")
        self.status = status
        self.save()

    def reject(self):
        status = Status.objects.get(name="REJECTED")
        self.status = status
        self.save()


    def get_api_url(self):
        return reverse("api_show_presentation", kwargs={"id": self.id})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ("title",)  # Default ordering for presentation

    @classmethod #When you create a new Presentation entity, its default status should be "SUBMITTED". Since Status is inside an aggregate, we shouldn't use it from the view function.
    def create(cls, **kwargs): #(**kwargs). This means it can accept any number of named arguments, and they will be packed into a dictionary called kwargs
        kwargs["status"] = Status.objects.get(name="SUBMITTED") #nside the method, it sets the value of the status key in the kwargs dictionary to a Status object obtained by querying the database for a Status object with the name "SUBMITTED". This ensures that the default status for the created object is set to "SUBMITTED".
        presentation = cls(**kwargs) #It then creates an instance of the class (cls) using the unpacked kwargs. This line essentially creates a new object of the class with the provided attributes. After creating the object, it saves it to the database.
        presentation.save()
        return presentation


# ustom Class Method create: This is a class method named create decorated with @classmethod.
#  It overrides the default create method provided by Django's models.Model.
#  This method is designed to simplify the creation of new Presentation instances by automatically...
#  setting the default status to "SUBMITTED" before saving the object to the database.

# the content, which is a dictionary, is passed from api views presentation  to kwargs, which can take many arguments
# Here's how it works:
# In your JSON data, include "created": true along with other attributes you want to set for the new presentation.

# When the create method is called, it will receive created=true as part of kwargs.

# The create method then uses **kwargs to unpack the keyword arguments and create a new Presentation instance with the provided attributes.

# When the presentation.save() method is called, it will save the new presentation instance to the database, including the created attribute if it's a valid field in the Presentation model.

# So, if you want to include "created": true in the JSON data sent to create a presentation, and you want it to be stored once it goes through presentation.save(), ensure that the created attribute is defined as a field in your Presentation model. If it's not a field in the model, you'll need to handle it separately in your code logic.
