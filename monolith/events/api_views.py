from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data


class ConferenceListEncoder(ModelEncoder): #It ensures that the queryset of conferences is converted into a dictionary or list format that can be serialized to JSON properly.
    model = Conference     # This encoder is specifically designed for serializing instances of the Conference model.
    properties = [
    "name",
    "id",
    # "href", all objects with a method named "get_api_url" on them should include it by default by adding it in the modelencoder in json.py
        #conference object has the get_api_url function tha is called by o.get
    ]

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):                    # gets the extra data, the state's abbreviation,
        return {"state": o.state.abbreviation}      # and returns it in a dictionary.
    #return value gets used by ModelEncoder to update its return..
    # ...dictionary with whatever extra values you decided to put into it.

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",        ###a json encoder transforms from python string to json string
        "starts",               #JSON has no representation for date and time
        "ends",                 #because of that we turn dates and times
        "created",              #into a format called "ISO formatted strings" using the ModelEncoder in json.py, isoformat() does that for us
        "updated",
        "location",   #needs its own encoder since it is its own object, we know this because it is a foreign key in Coference Model
    ]
    encoders = { ## tell the ModelEncoder that we want to use that specific encoder for the "location" property since location is its own object
        "location": LocationListEncoder()
    }

## start of conference list
@require_http_methods(["GET", "POST"]) #it only allows GET and POST request in this method
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()  # a queryset, which is associated with database queries and fetches data from the database
        return JsonResponse(                    #we need to change the query into a list Since JsonResponse only accepts dics or lists, which means we need a Query encoder  in json.py in commons folder
            {"conferences": conferences},  #the value conferences is an instance of the conferences queryset.
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict(from insomnia)
        try: #we have something like "location": conference.location  and json  doens't know what to do with that value since it is an object....
            location = Location.objects.get(id=content["location"]) #retrieve a Location object from the database based on the id(was automtically made ) provided in the JSON data. It assumes there's a Django model named Location with a field named id. location = 1
            content["location"] = location  #After retrieving the Location object(instance), it replaces the conference.location  in the content dictionary with the actual Location object, which in this case its only the id (1). This way, subsequent code can work directly with the Location object instead of just its id.
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder, #encoders transform python list and dics to JSON format
            safe=False, #Django needs safe =False when we need to use our own encoders and not the default one which can only encode dic and lists.
        )
    """ ^^^^
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.
    old code...
    response = []
    conferences = Conference.objects.all()
    for conference in conferences:
        response.append(
            {
                "name": conference.name,
                "href": conference.get_api_url(),
            }
        )
    return JsonResponse({"conferences": response})
    """
### end of conference list

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)           ##note: we start with detail views to use the encoders, then list views.
        weather = get_weather_data(conference.location.city, conference.location.state.abbreviation,) #the Third Party API is only used for GET not POST(only one where we need to access dictionary, such as content in get_photo). We are passing the city and the state to the get_weather function that is found in acls.py. no need to access
                                                            #so we end up with a variable that has a dic,  weather = {"description": scattered clouds, "temp": 42.55}
        return JsonResponse(
            {"conference": conference, "weather": weather},  #passes a Python object and a dictionary, conference and weather to our ConferenceDetailEncoder..
            encoder=ConferenceDetailEncoder,  # ...which converts the object and the dic into a JSON Format(still python though)
            safe =False,  #lets JsonResponse to use our encoder instead fo the default(JsonEncoder), then json.dumps function in JsonResponse transforms the Python(Json Format) to JSON string and sends an http response
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


    """ ^^
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href. old code...
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        {
            "name": conference.name,
            "starts": conference.starts,
            "ends": conference.ends,
            "description": conference.description,
            "created": conference.created,
            "updated": conference.updated,
            "max_presentations": conference.max_presentations,
            "max_attendees": conference.max_attendees,
            "location": {
                "name": conference.location.name,
                "href": conference.location.get_api_url(),
            },
        }
    )"""


@require_http_methods(["GET", "POST"]) #it only allows GET and POST request in this method
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()      #gets all of the locations
        return JsonResponse(                    #returns the locations in a JsonResponse
            {"locations": locations },
            encoder = LocationListEncoder
        )
    else:
        content = json.loads(request.body)             #converts the JSON Post request into a pytho dictionary, body comes from the imput data in the server insomnia
        try: #JSON doens't know what to do with the model state, which is in Location Model, so we go either with the abbreviation or the name in the state model. at this stage we have "state" : location.state, which JSON doesn't know waht to do with State model so we fix it here
            state = State.objects.get(abbreviation=content["state"])    #retrieves a State object from the database based on the abbreviation provided in the JSON data. It assumes there's a Django model named State with a field named abbreviation.
            content["state"] = state                #After retrieving the State object(instance), it replaces the location.state  in the content dictionary with the actual State object, which in this case its only the abbreviation("CA"). This way, subsequent code can work directly with the State object instead of just its abbreviation.

        except State.DoesNotExist: #  handle the error by returning an error message and a status code of 400 EX. if someone sends the state abbreviation "ZZ" or "currants-and-whey"? then it will send status 400
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status = 400,
            )                                                           #the line of code below adds the picture url to the content dic that we are requesting from Insomnia
        content["picture_url"]= get_photo(content["city"], content["state"].abbreviation) #function will return a url.. #note .abbreviation is found in the model.py events inside State. Passing the arguments city and the abbreviation field in the State model to the function get_photo in acls.py so that function over there can find the correct URL
        location = Location.objects.create(**content)  #uses that dictionary to create a new location
        return JsonResponse(                           #returns the new location
            location,
            encoder=LocationDetailEncoder,
            safe=False,                             #Make it a POST request pointing to http://localhost:8000/api/locations/.
        )                                           #From the Body dropdown, choose JSON
    """^^^
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.
    old code:
    locations = Location.objects.all()                  #get my information list of ALL locations
    response =[]
    for location in locations:
        response.append(
            {                                           # convert info to an array of dicts
                "name": location.name,
                "href": location.get_api_url(),
            }
        )

    return JsonResponse({"locations": response})        #return my information
    """
## location detail start


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder = LocationDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})   #it indicates if anythong got deleted,
                                                # in the browser it returns {"deleted": true}, or {"deleted":false} if no deleted items
    else:
        content = json.loads(request.body) #copied from create
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"]) #it is going to the state object and getting the specified content["state"]. similar to state.content["state"] .Ex.  giving us state = "CA"
                content["state"] = state #..now that it is a "State" instance we can assign it to content["state"]
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id) # copied from get detail
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.
    old code.....
    location = Location.objects.get(id=id)
    response = {
        "name": location.name,
        "city": location.city,
        "room_count": location.room_count,
        "created": location.created,
        "updated": location.updated,
        "state": location.state.abbreviation,    ##JSON doens't know what to do with the model state, which is in Location Model, so we go either with the abbreviation or the name in the state model
    }

    return JsonResponse({"location": response})
    """
##location detail end
