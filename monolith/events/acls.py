from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

#return an url to our  api_list_location function
def get_photo(city, state):                                                     #for line 7(next one). In fearsome-frontend we put the params in a dictionary to make it more organized
    url = f"https://api.pexels.com/v1/search?query={city},{state}&per_page=1"  #the third party api requires some parameters(params) to get what we want so we pass city and state. it can be other things such as oceans, tigers or any other query/string. the one is a fixed value so we do it directly.
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    response = requests.get(url, headers=headers) #we need the url(in this case we also added the parameters state and city in the url) and then the authorization key to acces the API url
    data = json.loads(response.content) #this converts(parses) the jason string(response.content) into a python dictionary
    # print (data["photos"][0]["src"]) #..then we get in deep in the now python dictionary and the first lists,[0],to only get one "original" image. we can just go to the next line and modify it to get differernt sizes if we change "original" to another key found in the "src" dictionary such as ["medium"] etc.
    return  data["photos"][0]["src"]["original"] #returns only one picture url from all the ones found in photos,


def get_weather_data(city, state):  #proper way to do it is to have a dic for headers, which inside have the authorization value needed byt the endpoint API.Also, params which is a dic that has parameters needed to pull the data from the TP API such as "q": f"{city},{state},US", "limit": 1,
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url) #response is an object since it represents the entire HTTP response from the server
    content = json.loads(response.content) #we access the content, and it goes form JSON string to Python dic
    # print(content)
    # print(content[0]["lat"])
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    #usually  the parameters(params) go on in a dictionary to stay orgainzed, unlike what i did here
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url) #gets an object from the TP API
    data = json.loads(response.content) #accesses the content of the object converts the JSON string to a Python dic
    print(data["weather"][0]["description"]) #access the dic to get the description
    try:
        return { "description" : data["weather"][0]["description"], "temp": data["main"]["temp"],}#return the description and the temp , which be send as a python dic,  {"description": scattered clouds, "temp": 42.55}
    except (KeyError, IndexError):
        return None


# Documentation on How Third Party APIs are integrated:
# https://requests.readthedocs.io/en/latest/user/quickstart/

#BETTER WAY TO WRITE OUR CODE AND STAY ORGANIZED:
# def get_photo(city, state):
#     headers = {"Authorization": PEXELS_API_KEY}
#     params = {
#         "per_page": 1,
#         "query": f"downtown {city} {state}",
#     }
#     url = "https://api.pexels.com/v1/search"
#     response = requests.get(url, params=params, headers=headers)
#     content = json.loads(response.content)
#     try:
#         return {"picture_url": content["photos"][0]["src"]["original"]}
#     except (KeyError, IndexError):
#         return {"picture_url": None}


# def get_weather_data(city, state):
#     params = {   #this are required by the weather API to be able to pull the data, and later be used by the other Third Party API to find the specific weather
#         "q": f"{city},{state},US",
#         "limit": 1,
#         "appid": OPEN_WEATHER_API_KEY,
#     }
#     url = "http://api.openweathermap.org/geo/1.0/direct"
#     response = requests.get(url, params=params)
#     content = json.loads(response.content)

#     try:
#         latitude = content[0]["lat"]
#         longitude = content[0]["lon"]
#     except (KeyError, IndexError):
#         return None

#     params = {
#         "lat": latitude,
#         "lon": longitude,
#         "appid": OPEN_WEATHER_API_KEY,
#         "units": "imperial",
#     }
#     url = "https://api.openweathermap.org/data/2.5/weather"
#     response = requests.get(url, params=params)
#     content = json.loads(response.content)

#     try:
#         return {
#             "description": content["weather"][0]["description"],
#             "temp": content["main"]["temp"],
#         }
#     except (KeyError, IndexError):
#         return None
