from django.db.models import QuerySet #will import the Queryset class to checks if the object is an instance of it.
from json import JSONEncoder
from datetime import datetime


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet): #if o is an instance of a QuerySet, just turn it into
            return list(o)          # a normal list rather than the fancy list that it already is.
        else:
            return super().default(o)

class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):    # if o is an instance of datetime
            return o.isoformat()
        else:
            return super().default(o)
#note we need to add DateEncoder before JSONEncoder in ModelEncoder

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder): #allows objects to be converted to  basic data types(str, int, dic, list, ) then JsonResponse takes ...
    encoders = {}                                               #... these basic data types and seriealizes them into JSON strings, which are sent as an HTTP response
    def default(self, o):           # 'self' refers to the current instance of the encoder (e.g., ConferenceListEncoder since its a subclass of ModelEncoder), and 'o'(accepts the (one)queryset "conference" that is inside JsonResponse in api_list_conferences.
                                    # This method is responsible for converting the object 'o' into a format suitable for JSON serialization.
                                                 # the first IF checks if the incoming single conference instance/object (conference inside JsonResponse)in the o parameter is an
        if isinstance(o, self.model):           #instance of the class self.model(),"Conference", property. It basically check if a single conference object within the queryset is an instance of Conference model
            d = {}                              #will hold the property names as keys and the property values as values
            if hasattr(o,"get_api_url"):        # if o (individual object within the queryset that comes from inside jsonResponse) has the attribute get_api_url
                d["href"] = o.get_api_url()     #then add its return value to the dictionary with the key "href"
            for property in self.properties:    # inside ConferenceListEncoder, self.properties is accessed to determine which properties of the Conference model should be included in the JSON response.
                value = getattr(o, property)     #built-in function that retrieves the value of the attribute named property from the object o  (conference instance)
                if property in self.encoders:     # Checks if there is a custom encoder defined for the current property.
                    encoder = self.encoders[property]     # Retrieves the custom encoder associated with the current property from the self.encoders dictionary. We mostly have only been working with ConferenceListEndodert, but An example is for location in ConferenceDetailEncoder
                    value = encoder.default(value)     # Applies the custom encoder to the current property value.
                d[property] = value             #  Add the property and its value to the dictionary.
            #only LocationDetailEncoder has get_extra_data function
            d.update(self.get_extra_data(o)) # an example of how this works. it starts at  api_list_location on the JsonResponse when the encoder LocationDetailEncoder is called . The object "location" is passes as an argument  "o"  to the default function in json.py ..
            return d                            #... then it goes to that LocationDetailEncoder(which is a subclass of ModelEncoder therefore inherits its methods).It then retrieves the data from the function get_extra_data found in the subclass and adds it to the dictionary(ex. TX)
        else:                                   # otherwise,
            return super().default(o)           # ensures a robust and flexible approach to JSON encoding, where custom handling is applied where necessary, but default behavior is always available as a fallback option...
                                                #...If only strings (or any other JSON-serializable basic data types like integers, floats, lists, dictionaries, etc.) are passed to the ModelEncoder, the super().default(o) call will handle them.
    def get_extra_data(self, o):  #Subclasses like LocationDetailEncoder can override this method to provide custom extra data when needed, while other subclasses that
        return {}           #..don't require extra data can simply use the default implementation provided by ModelEncoder


# What Encoders Do:
# Encoders, such as ConferenceDetailEncoder , are necessary because the default JsonResponse in Django relies on Python's json module for serialization.
# However, the default JsonEncoder provided by Python's json module can only convert basic data types like dictionaries, lists, strings, and integers into JSON format.
# It cannot handle more complex Python objects/classes such as instances of custom classes like our Conference model, so we use our own and not the default at all

# Therefore, we create our own custom JSON encoder to convert Python objects and their properties into a Python format that is
# compatible with JSON. This involves transforming complex objects like Django model instances into simpler Python data types
# such as dictionaries, lists, strings, integers, tuples, and booleans.

# Once this transformation is done, the resulting data, still in Python format but structured to fit the requirements of JSON syntax,
# can be serialized into a JSON string using JsonResponse(utilizes json.dumps()). This allows us to send structured data, including Django model instances,
# as JSON responses from our Django views.


# You can see  that  JSONEncoder class supports the data types:    **that's why we make our own encoder since this does not support objects
# dict (a dictionary),
# list and tuple,
# str (a string),
# int and float (for numbers),
# True and False (the Boolean values)
# None
# 	##making our own encoder so it can take in classes(our models)
# The reason we have to write a dictionary is because Json Encoder doesn’t support classes.
# We have to write our own version of the Json Encoder class so  json.dumps(a function inside JsonResponse) is able to change our models into JSON string.


#We have the super class JsonEncoder, then ModelEncoder(inherits JsonEncoder properties) , and then the lowest sublcass ConferenceDetailEncoder, LocationDetailEncoder, etc.


# When we say "JSON format," we mean that the data is structured according to the syntax rules of JSON (JavaScript Object Notation),
# but it's still in Python data types (dictionaries, lists, strings, integers, etc.). JsonResponse takes this Python data and converts
# it into an actual JSON string, which is then sent as the response over HTTP. So, JsonResponse transforms the Python data into JSON
# format during the serialization process.


# Summary:
# The custom encoder prepares the data in the correct Python format for serialization.
# JsonResponse then utilizes json.dumps() to serialize this Python-formatted data into a JSON string.
# Finally, the JSON string is sent as the HTTP response body by JsonResponse.
